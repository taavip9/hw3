import java.util.ArrayList;
import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> stacklist;

   public static void main (String[] argum) {

      DoubleStack a = new DoubleStack();
      System.out.println(a.interpret(" 2. 15. - "));


   }

   DoubleStack() {
      stacklist = new LinkedList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack newstack = new DoubleStack();
      newstack.stacklist = (LinkedList<Double>) stacklist.clone();

      return newstack;
   }

   public boolean stEmpty() {

      if (stacklist.size() == 0) {
         return true;
      } else {
         return false;
      }
   }

   public void push (double a) {

      stacklist.push(a);
   }

   public double pop() {

      if (stEmpty())
      {
         throw new RuntimeException("Not enough elements to perform action");
      } else {
         return stacklist.pop();
      }


   } // pop

   public void op (String s) {

      double converelem2 = stacklist.pop();
      double converelem1 = stacklist.pop();


      if (s.equals("+")) {
         double result = converelem1 + converelem2;
         push(result);

      } else if (s.equals("*")) {
         double result = converelem1 * converelem2;
         push(result);

      } else if (s.equals("/")) {
         double result = converelem1 / converelem2;
         push(result);

      } else if (s.equals("-")) {
         double result = converelem1 - converelem2;
         push(result);

      } else {
         throw new RuntimeException("Operator " +s+ " is invalid. Usable operators are +, -, / and *");
      }
   }
  
   public double tos() {
      if (stEmpty()){
         throw new RuntimeException("Not enough elements to perform action");
      }
      else {
         return stacklist.getFirst().doubleValue();
      }
   }

   @Override
   public boolean equals (Object o) {
      if (stacklist.equals(((DoubleStack) o).stacklist))
      {
         return true;
      }else {
         return false;
      }
   }

   @Override
   public String toString() {

      StringBuffer buff = new StringBuffer();

      for (int i = 0; i < stacklist.size();i++)
      {

         buff.append(stacklist.get((stacklist.size()-1)));
         buff.append(", ");
      }

      buff.toString();

      return buff.toString();
   }

   public static double interpret (String pol) {


      DoubleStack interpstck = new DoubleStack();

      String[] elements = pol.trim().split("\\s+");

      int i = 0;
      int u = 0;

      for (String element : elements) {
         try {
            interpstck.push(Double.valueOf(element));
            i++;
         } catch (Exception e) {
            if(interpstck.stacklist.size()<2 || interpstck.stEmpty()){
               throw new RuntimeException("Not enough elements to perform equation "+pol);
            } else if (!element.equals("+") && !element.equals("/") && !element.equals("*") && !element.equals("-")){
               throw new RuntimeException("Unauthorized character in equation "+pol);
            }
            interpstck.op(element);
            u++;
         }
      }

      if (i - 1 != u) {
         throw new RuntimeException("Equation "+pol+" is out of balance, please verify inserted numbers and operators");
      }

      return interpstck.pop();
   }

}